package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.http.util.EntityUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import play.Application;
import play.api.mvc.Session;
import play.db.Database;
import play.db.Databases;
import play.db.evolutions.Evolutions;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static play.test.Helpers.*;
import static play.test.Helpers.route;

//@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerTest extends WithApplication{

//    @Deployment
//    public static JavaArchive createDeployment() {
//        return ShrinkWrap.create(JavaArchive.class)
//                .addClass(UserController.class)
//                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
//    }
    public static Application app;
    public static Database database;
    public Http.RequestBuilder requestBuilder = new Http.RequestBuilder();


    @BeforeClass
    public static void setUpApp(){
        Config playConfig = ConfigFactory.load();
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.driver", "org.postgresql.Driver");
        settings.put("db.default.url", playConfig.getString("db.default.url"));
        settings.put("db.default.username", "postgres");
        settings.put("db.default.password", "");
        settings.put("play.evolutions.autoApply", "true");
        settings.put("db.default.autoApply","true");
        settings.put("ebean.default", "models.*");
        app = play.test.Helpers.fakeApplication();
        database = app.injector().instanceOf(Database.class);
        Evolutions.applyEvolutions(database);
        Helpers.start(app);
    }

    @Test
    public void Asignup() {
        //mapper
        ObjectMapper mapper = new ObjectMapper();

        //Request
        HashMap<String, String> testBody = new HashMap<String,String>();
        testBody.put("email","dipta006@yahoo.co.id");
        testBody.put("password","123456");
        testBody.put("type","Customer");
        testBody.put("name","Dipta Laksmana");
        JsonNode jsonNode = mapper.valueToTree(testBody);
        requestBuilder.method(POST)
                    .bodyJson(jsonNode)
                    .uri("/api/signup");

        //testing
        Result result = route(app,requestBuilder);

        assertEquals(OK, result.status());
    }

    @Test
    public void Bsignin() {
        //mapper
        ObjectMapper mapper = new ObjectMapper();

        //Request
        HashMap<String, String> testBody = new HashMap<String,String>();
        testBody.put("email","dipta006@yahoo.co.id");
        testBody.put("password","123456");
        JsonNode jsonNode = mapper.valueToTree(testBody);
        requestBuilder
                .method(POST)
                .bodyJson(jsonNode)
                .uri("/api/signin");

        //testing
        Result result = route(app,requestBuilder);
        assertEquals(OK, result.status());
        assertEquals("dipta006@yahoo.co.id",result.session().get("email"));
    }

    @Test
    public void Cme() {
        //Request
        requestBuilder
                .method(GET)
                .uri("/api/user/me")
                .session("email","dipta006@yahoo.co.id");

        //testing
        Result result = route(app,requestBuilder);

        assertEquals(OK, result.status());
    }

    @Test
    public void Dlogout() {
        //Request
        requestBuilder
                .method(GET)
                .uri("/api/signout")
                .session("email","dipta006@yahoo.co.id");

        //testing
        Result result = route(app,requestBuilder);

        assertEquals(OK, result.status());
    }
    @AfterClass
    public static void shutDownApp(){
        Evolutions.cleanupEvolutions(database);
        Helpers.stop(app);
    }
}
