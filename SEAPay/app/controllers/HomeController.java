package controllers;

import play.mvc.*;
import play.mvc.Results.*;
import play.mvc.Controller;

public class HomeController extends Controller {
    public Result index() {
        return ok("OK");
    }
}
