package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import play.data.FormFactory;
import play.data.Form;

import util.ApiResponse;

import javax.inject.Inject;

import forms.ProductCreationForm;

import service.product.ProductApplicationService;

public class ProductController extends Controller {
    private FormFactory formFactory;
    private ApiResponse apiResponse;

    @Inject
    public ProductController(FormFactory formFactory) {
        this.apiResponse = ApiResponse.getInstance();
        this.formFactory = formFactory;
    }

    // POST /api/product/create/
    public Result create() {
        Form<ProductCreationForm> productCreationForm = formFactory.form(ProductCreationForm.class).bindFromRequest();

        return ProductApplicationService.addProduct(productCreationForm.get());
    }

    // GET /api/product/remove/:product_id
    public Result remove(Long product_id) {
        return ProductApplicationService.removeProduct(product_id);
    }
}
