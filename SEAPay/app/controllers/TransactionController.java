package controllers;

import forms.PaymentForm;
import forms.TopUpForm;
import forms.TransferForm;

import javax.inject.Inject;

import java.util.List;

import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import service.transaction.TransactionApplicationService;

import util.ApiResponse;


public class TransactionController extends Controller {
    private FormFactory formFactory;
    private ApiResponse apiResponse;

    @Inject
    public TransactionController(FormFactory formFactory) {
        this.apiResponse = ApiResponse.getInstance();
        this.formFactory = formFactory;
    }

    // POST /api/transfer
    @Security.Authenticated(Authentication.class)
    public Result transfer(){
        Form<TransferForm> transferForm = formFactory.form(TransferForm.class).bindFromRequest();
        if(transferForm.hasErrors()){
            apiResponse.success = false;
            List<ValidationError> validationErrorList = transferForm.errors();
            String errorMessage = "";
            for (ValidationError obj : validationErrorList){
                errorMessage += String.format(" %s %s %s,", obj.key(),obj.message(),(obj.arguments().toArray().length == 0 ? "" : obj.arguments() )); //Formatting error message
            }
            apiResponse.message = "Error:" + errorMessage;
            return forbidden(apiResponse.toJson());
        }

        return TransactionApplicationService.transfer(transferForm.get());
    }

    // POST /api/topup
    @Security.Authenticated(Authentication.class)
    public Result topUp(){

        Form<TopUpForm> topUpForm  = formFactory.form(TopUpForm.class).bindFromRequest();
        if(topUpForm.hasErrors()){
            apiResponse.success = false;
            List<ValidationError> validationErrorList = topUpForm.errors();
            String errorMessage = "";
            for (ValidationError obj : validationErrorList){
                errorMessage += String.format(" %s %s %s,", obj.key(),obj.message(),(obj.arguments().toArray().length == 0 ? "" : obj.arguments() )); //Formatting error message
            }
            apiResponse.message = "Error:" + errorMessage;
            return forbidden(apiResponse.toJson());
        }
        return TransactionApplicationService.topUp(topUpForm.get());
    }

    // POST /api/pay
    @Security.Authenticated(Authentication.class)
    public Result payment(){
        Form<PaymentForm> paymentForm = formFactory.form(PaymentForm.class).bindFromRequest();
        if(paymentForm.hasErrors()){
            apiResponse.success = false;
            List<ValidationError> validationErrorList = paymentForm.errors();
            String errorMessage = "";
            for (ValidationError obj : validationErrorList){
                errorMessage += String.format(" %s %s %s,", obj.key(),obj.message(),(obj.arguments().toArray().length == 0 ? "" : obj.arguments() )); //Formatting error message
            }
            apiResponse.message = "Error:" + errorMessage;
            return forbidden(apiResponse.toJson());
        }

        return TransactionApplicationService.payment(paymentForm.get());
    }

    // GET /api/history/
    @Security.Authenticated(Authentication.class)
    public Result transactionHistory(){
        return TransactionApplicationService.historyTransaction(session().get("email"));
    }


}
