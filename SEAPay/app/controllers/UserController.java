package controllers;

import forms.AdminSignupForm;
import forms.MerchantForm;
import play.data.validation.ValidationError;
import play.mvc.*;
import play.data.FormFactory;
import play.data.Form;

import forms.UserSignupForm;
import forms.UserSigninForm;
import forms.SetProfileForm;

import javax.inject.Inject;

import service.user.UserApplicationService;

import util.ApiResponse;
import util.FormError;

import java.util.List;

public class UserController extends Controller {
    private FormFactory formFactory;
    private ApiResponse apiResponse;

    @Inject
    public UserController(FormFactory formFactory) {
        this.apiResponse = ApiResponse.getInstance();
        this.formFactory = formFactory;
    }

    // POST /api/signup
    public Result signup() {
        Form<UserSignupForm> userSignupForm = formFactory.form(UserSignupForm.class).bindFromRequest();
        
        if (userSignupForm.hasErrors()) {
            String errorMessage = FormError.getErrorMessage(userSignupForm);

            apiResponse.success = false;
            apiResponse.message = "Error:" + errorMessage;
            return forbidden(apiResponse.toJson());
        }

        // Check if it is a merchant or not
        if(!userSignupForm.get().getType().equalsIgnoreCase("merchant")) {
            return UserApplicationService.signup(userSignupForm.get());
        // If Merchant is appliying register
        }else{
            Form<MerchantForm> merchantSignupForm = formFactory.form(MerchantForm.class).bindFromRequest();
            if (merchantSignupForm.hasErrors()) {
                String errorMessage = FormError.getErrorMessage(userSignupForm);

                apiResponse.success = false;
                apiResponse.message = "Error:" + errorMessage;
                return forbidden(apiResponse.toJson());
            }

            return UserApplicationService.signup(merchantSignupForm.get());

        }
    }

    // POST /api/signin
    public Result signin() {
        Form<UserSigninForm> userSigninForm = formFactory.form(UserSigninForm.class).bindFromRequest();

        if (userSigninForm.hasErrors()) {
            String errorMessage = FormError.getErrorMessage(userSigninForm);

            apiResponse.success = false;
            apiResponse.message = "Error:" + errorMessage;
            return forbidden(apiResponse.toJson());
        }

        Result result = UserApplicationService.signin(userSigninForm);
        if(result.status() == OK) {
            return result.addingToSession(request(),"email",userSigninForm.get().getEmail());
        }else{
            return result;
        }
    }

    // GET /api/user/me
    @Security.Authenticated(Authentication.class)
    public Result me() {
        String userEmail = session().get("email");

        return UserApplicationService.userDetail(userEmail);
    }

    // GET /api/signout
    @Security.Authenticated(Authentication.class)
    public Result logout() {
        // Clear the session (Deprecated as for Play 2.7)
        // https://www.playframework.com/documentation/2.7.x/api/java/play/mvc/Http.Session.html#clear--
        session().clear();

        apiResponse.success = true;
        apiResponse.data = null;
        return ok(apiResponse.toJson());
    }

    // POST /api/admin
    public Result adminSignup(){
        Form<AdminSignupForm> adminSignupForm = formFactory.form(AdminSignupForm.class).bindFromRequest();
        
        return UserApplicationService.adminSignup(adminSignupForm.get());
    }

    // POST /api/set_profile
    @Security.Authenticated(Authentication.class)
    public Result setProfile() {
        String userEmail = session().get("email");

        Form<SetProfileForm> setProfileForm = formFactory.form(SetProfileForm.class).bindFromRequest();

        if (setProfileForm.hasErrors()) {
            String errorMessage = FormError.getErrorMessage(setProfileForm);

            apiResponse.success = false;
            apiResponse.message = "Error:" + errorMessage;
            return forbidden(apiResponse.toJson());
        }

        return UserApplicationService.setProfile(userEmail, setProfileForm);
    }
}
