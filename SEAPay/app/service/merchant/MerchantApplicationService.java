package service.merchant;

import models.user.Merchant;
import models.user.Users;
import play.mvc.Result;
import util.ApiResponse;

import static play.mvc.Results.*;

public class MerchantApplicationService {
    public static ApiResponse apiResponse = ApiResponse.getInstance();

    public static Result getMerchantList(){ //For user
        apiResponse.success = true;
        apiResponse.data = MerchantModelService.getMerchantList();
        return ok(apiResponse.toJson());
    }

    public static Result getMerchantDetail(Long merchantId){
        if(!isMerchantExist(merchantId)) {
            apiResponse.success = false;
            apiResponse.message = "Merchant Not Found";
            return notFound(apiResponse.toJson());
        }

        apiResponse.success = true;
        apiResponse.data = MerchantModelService.getMerchantDetail(merchantId);
        return ok(apiResponse.toJson());
    }

    public static Result acceptMerchant(Long merchantId, String emailLoggedIn, boolean isAccepted){
        if(!isAdmin(emailLoggedIn)){
            apiResponse.success = false;
            apiResponse.message = "You are not admin";
            return forbidden(apiResponse.toJson());
        }

        if(!isMerchantExist(merchantId)) {
            apiResponse.success = false;
            apiResponse.message = "Merchant Not Found";
            return notFound(apiResponse.toJson());
        }

        apiResponse.success = true;
        apiResponse.data = (isAccepted ? MerchantModelService.acceptMerchant(merchantId) :
                MerchantModelService.declineMerchant(merchantId));
        return ok(apiResponse.toJson());
    }

    private static boolean isAdmin(String emailLoggedIn){
        Users users = Users.findByEmail(emailLoggedIn);
        return users.getType().equalsIgnoreCase("Admin");
    }

    private static boolean isMerchantExist(Long merchantId){
        Merchant merchant = Merchant.findByMerchantId(merchantId);
        return (merchant != null);
    }
}
