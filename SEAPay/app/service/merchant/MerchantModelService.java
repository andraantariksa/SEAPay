package service.merchant;

import models.user.Merchant;

import java.util.List;

public class MerchantModelService {
    public static List<Merchant> getMerchantList(){
        return Merchant.findMerchantActiveList();
    }

    public static Merchant getMerchantDetail(Long merchantId){
        return Merchant.findMerchantDetailByMerchantId(merchantId);
    }

    public static List<Merchant> acceptMerchant(Long merchantId){
        Merchant merchant = Merchant.findByMerchantId(merchantId);
        merchant.setHasSeenByAdmin(true);
        merchant.setAccepted(true);

        merchant.update();

        return Merchant.findMerchantProposal();
    }

    public static List<Merchant> declineMerchant(Long merchantId){
        Merchant merchant = Merchant.findByMerchantId(merchantId);
        merchant.setHasSeenByAdmin(true);
        merchant.setAccepted(false);

        merchant.update();

        return Merchant.findMerchantProposal();
    }
}
