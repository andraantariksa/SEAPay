package service.product;

import forms.CartProductForm;
import forms.ProductCreationForm;
import models.product.Product;

import java.util.List;

public class ProductModelService {
    public static List<Product> addProduct(ProductCreationForm productCreationForm){
        Product product = new Product(productCreationForm);
        product.insert();

        return Product.findByMerchantId(productCreationForm.getMerchantId());
    }

    public static List<Product> removeProduct(Long productId){
        Product product = Product.findByProductId(productId);
        Long merchantId = product.getMerchantId();

        product.delete();

        return Product.findByMerchantId(merchantId);
    }

    public static void updateProductStockFromCart(List<CartProductForm> cartProductFormList){
        for (CartProductForm cartProductForm :
                cartProductFormList) {
            Product product = Product.findByProductId(cartProductForm.getProductId());
            product.reduceStock(cartProductForm.getAmmount());

            product.update();
        }
    }

    public static List<Product> obtainMerchantProduct(Long merchantId){
        return Product.findByMerchantId(merchantId);
    }
}
