package service.user;

import forms.AdminSignupForm;
import forms.MerchantForm;
import forms.UserSigninForm;
import forms.UserSignupForm;
import forms.SetProfileForm;
import models.user.Customer;
import models.user.Merchant;
import models.user.Users;
import play.data.Form;
import util.ApiResponse;

import forms.UserSigninForm;

import play.mvc.Result;

import static play.mvc.Controller.session;
import static play.mvc.Results.forbidden;
import static play.mvc.Results.ok;

public class UserApplicationService {

    public static ApiResponse apiResponse = ApiResponse.getInstance();

    public static Result signin(Form<UserSigninForm> userSigninForm){
        // Extracting the form data
        UserSigninForm userSigninFormData = userSigninForm.get();

        String loginEmail = userSigninFormData.getEmail();
        String loginPassword = userSigninFormData.getPassword();

        // Find user
        Users foundUser = Users.findByEmail(loginEmail);

        // If user email is not found
        if (foundUser == null) {
            apiResponse.success = false;
            apiResponse.message = "Wrong email or password";
            return forbidden(apiResponse.toJson());
        }

        // If user password is invalid
        if (!foundUser.isPasswordEqual(loginPassword)) {
            apiResponse.success = false;
            apiResponse.message = "Wrong email or password"; // Wrong password
            return forbidden(apiResponse.toJson());
        }

        // If user type is merchant
        if(foundUser.getType().equalsIgnoreCase("merchant")){
            Merchant merchant = Merchant.findByUserId(foundUser.getId());
            
            if(!merchant.isHasSeenByAdmin()){
                apiResponse.success = false;
                apiResponse.message = "Not confirmed yet"; // Wrong password
                return forbidden(apiResponse.toJson());
            }

            if(!merchant.isAccepted()){
                apiResponse.success = false;
                apiResponse.message = "Proposal rejected"; // Wrong password
                return forbidden(apiResponse.toJson());
            }
        }

        apiResponse.success = true;
        apiResponse.message = "";
        return ok(apiResponse.toJson());
    }

    public static Result signup(UserSignupForm userSignupForm){
        String newUserEmail = userSignupForm.getEmail();

        // If newUserEmail is already exists
        if (Users.isEmailExists(newUserEmail)) {
            apiResponse.success = false;
            apiResponse.message = "Email already exists";
            return forbidden(apiResponse.toJson());
        }

        // Insert the user
        UserModelService.createUser(userSignupForm);
        apiResponse.success = true;
        return ok(apiResponse.toJson());
    }

    public static Result userDetail(String activeEmail){
        Users foundUser = Users.findByEmail(activeEmail);

        // If user email is not found
        if (foundUser == null) {
            apiResponse.success = false;
            apiResponse.message = "No user found";
            return forbidden(apiResponse.toJson());
        }

        apiResponse.success = true;
        apiResponse.data = foundUser.obtainUserDetails();
        return ok(apiResponse.toJson());
    }

    public static Result setProfile(String userEmail, Form<SetProfileForm> setProfileForm){
        SetProfileForm setProfileFormData = setProfileForm.get();

        String oldPassword = setProfileFormData.getOldPassword();
        String newPassword = setProfileFormData.getNewPassword();
        String newName = setProfileFormData.getName();
        
        Users user = Users.findByEmail(userEmail);
        Long userId = user.getId();

        // If user email is not found
        if (user == null) {
            apiResponse.success = false;
            apiResponse.message = "No user found";
            return forbidden(apiResponse.toJson());
        }

        // Verify the oldPassword
        if (!user.isPasswordEqual(oldPassword)) {
            apiResponse.success = false;
            apiResponse.message = "Wrong password";
            return forbidden(apiResponse.toJson());
        }

        // If the user is a merchant, then update the description
        if (user.getType().equals("Merchant")) {
            String description = setProfileForm.get().getDescription();

            Merchant userMerchant = Merchant.findMerchantDetailByUserId(userId);
            userMerchant.setDescription(description);
            userMerchant.save();
        }

        // Update user data
        user.setPassword(newPassword);
        user.setName(newName);
        user.save();

        apiResponse.success = true;
        apiResponse.data = user.obtainUserDetails();
        return ok(apiResponse.toJson());
    }

    public static Result adminSignup(AdminSignupForm adminSignupForm){
        String newAdminEmail = adminSignupForm.getEmail();

        // If newAdminEmail already used
        if(Users.isEmailExists(newAdminEmail)){
            apiResponse.success = false;
            apiResponse.message = "Email already exists";
            return forbidden(apiResponse.toJson());
        }

        UserModelService.createAdmin(adminSignupForm);
        apiResponse.success = true;
        return ok(apiResponse.toJson());
    }


}
