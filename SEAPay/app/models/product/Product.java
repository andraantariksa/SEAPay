package models.product;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import forms.ProductCreationForm;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebeaninternal.server.lib.util.Str;
import models.product.serializer.ProductSerializer;
import models.user.serializer.UserSerializer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "products")
@JsonSerialize(using = ProductSerializer.class) //For customize the JSON key
public class Product extends Model {

    @Id
    private Long productId;

    private Long merchantId;
    private String name;
    private BigDecimal price;
    private String description;
    private Integer stock;

    public Product(ProductCreationForm productCreationForm){
        this.merchantId = productCreationForm.getMerchantId();
        name = productCreationForm.getName();
        price = productCreationForm.getPrice();
        description = productCreationForm.getDescription();
        stock = productCreationForm.getStock();
    }

    public Long getProductId() {
        return productId;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public void reduceStock(Integer ammount){
        stock = stock-ammount;
    }

    private static final Finder<Long, Product> find = new Finder<>(Product.class);

    public static List<Product> findByMerchantId(Long merchantId){
        return find.query().where().eq("merchant_id",merchantId).findList();
    }

    public static Product findByProductId(Long productId){
        return find.query().where().eq("product_id",productId).findOne();
    }

    public static Product findByProductNameWithMerchantId(String productName,Long merchantId){
        return find.query().where().eq("merchant_id",merchantId).eq("name", productName).findOne();
    }

}
