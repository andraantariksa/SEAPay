package models.product.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.product.Product;

import java.io.IOException;

public class ProductSerializer extends StdSerializer<Product> {

    public ProductSerializer(){
        this(null);
    }

    protected ProductSerializer(Class<Product> t) {
        super(t);
    }

    @Override
    public void serialize(Product product, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("merchant_id",product.getMerchantId());
        jsonGenerator.writeNumberField("product_id",product.getProductId());
        jsonGenerator.writeStringField("name",product.getName());
        jsonGenerator.writeNumberField("price",product.getPrice().longValue());
        jsonGenerator.writeStringField("description",product.getDescription());
        jsonGenerator.writeNumberField("stock",product.getStock());
        jsonGenerator.writeEndObject();
    }
}
