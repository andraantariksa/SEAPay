package models.user;

import javax.persistence.*;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.ebean.*;

import forms.UserSignupForm;

// Bcrypt
import models.transaction.Wallet;
import models.user.serializer.UserSerializer;
import org.mindrot.jbcrypt.BCrypt;

// Configuration
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.Config;

import java.math.BigDecimal;

@Entity
@Table(name = "users")
@JsonSerialize(using = UserSerializer.class) //For customize the JSON key
public class Users extends Model {
    @Id
    private Long userId;
    private String email;
    private String password;
    private String name;
    private String type;
    private Long walletId;

    // Ignore the transient field
    @Transient
    private transient String bcryptSalt;
    @Transient
    private transient BigDecimal balance;
    @Transient
    private transient UserDetail userDetail;

    private static final Finder<Long, Users> find = new Finder<>(Users.class);

    public static Users findById(Long userId){
        return find.query().where().eq("user_id",userId).findOne();
    }

    public static Users findByEmail(String email) {
        return find.query().where().eq("email", email).findOne();
    }

    public static Users findByWalletId(Long walletId) {return find.query().where().eq("wallet_id",walletId).findOne();}

    public static Users findMerchantById(Long userId) {
        return find.query().where().eq("user_id",userId).eq("type","Merchant").findOne();
    }

    public static Users findCustomerById(Long userId) {
        return find.query().where().eq("user_id",userId).eq("type","Customer").findOne();
    }

    public static Users findAdminById(Long userId){
        return find.query().where().eq("user_id",userId).eq("type","Admin").findOne();
    }

    public static Users findByName(String name){
        return find.query().where().eq("name",name).findOne();
    }

    public Users(Long walletId) {
        // Getting Bcrypt salt
        Config playConfig = ConfigFactory.load();
        this.bcryptSalt = playConfig.getString("bcrypt.salt");

        // Add wallet_id
        this.walletId = walletId;
    }

    public boolean isPasswordEqual(String password) {
        try {
            return BCrypt.checkpw(password, this.password);
        } catch (Exception e) {
            // Password was not hashed correctly
            return false;
        }
    }

    public static boolean isEmailExists(String email) {
        return Users.findByEmail(email) != null;
    }

    public void fromUserSignupForm(UserSignupForm userSignupForm) {
        this.email = userSignupForm.getEmail();
        this.password = BCrypt.hashpw(userSignupForm.getPassword(), this.bcryptSalt);
        this.name = userSignupForm.getName();
        this.type=userSignupForm.getType();
    }

    public Users obtainUserDetails(){
        if(!type.equalsIgnoreCase("Admin")){
            Wallet wallet = Wallet.findById(this.walletId);
            setBalance(wallet.getBalance());
        }
        switch (this.type.toLowerCase()){
            case "admin":
                this.userDetail = Admin.findAdminDetailByUserId(userId);
                break;
            case "merchant":
                this.userDetail = Merchant.findMerchantDetailByUserId(userId);
                break;
            case "customer":
                this.userDetail = Customer.findCustomerDetailByUserId(userId);
                break;
            default:
                this.userDetail = null;
                break;
        }

        return this;
    }

    public Long getId() {
        return this.userId;
    }

    public String getEmail() {
        return this.email;
    }

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        Config playConfig = ConfigFactory.load();
        this.bcryptSalt = playConfig.getString("bcrypt.salt");
        this.password = BCrypt.hashpw(password, this.bcryptSalt);
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }
}
