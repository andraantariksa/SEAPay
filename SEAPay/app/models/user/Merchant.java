package models.user;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.ebean.Finder;
import io.ebean.Model;
import models.product.Product;
import models.transaction.Wallet;
import models.user.serializer.MerchantSerializer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "merchant")
@JsonSerialize(using = MerchantSerializer.class) //For Modify the JSON
public class Merchant extends Model implements UserDetail {

    @Id
    private Long merchantId;

    private Long userId;

    private String description;

    private boolean hasSeenByAdmin = false;

    private boolean isAccepted = false ;

    @Transient
    private transient String name;

    @Transient
    private transient List<Product> productList;

    @Transient
    private transient Long walletId;

    private static final Finder<Long, Merchant> find = new Finder<>(Merchant.class);

    public Merchant(Long userId){
        this.userId=userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static Merchant findByUserId(Long userId){
        return find.query().where().eq("user_id",userId).findOne();
    }

    public static Merchant findByMerchantId(Long merchantId) { return find.query().where().eq("merchant_id",merchantId).findOne();}

    public static List<Merchant> findMerchantProposal(){
        List<Merchant> merchantList =  find.query().where().eq("has_seen_by_admin",false).findList();
        for (Merchant obj :
                merchantList) {
            obj.obtainMerchantName();
        }
        return merchantList;
    }

    public static List<Merchant> findMerchantActiveList(){
        List<Merchant> merchantList =  find.query().where().eq("has_seen_by_admin",true).eq("is_accepted",true).findList();
        for (Merchant obj :
                merchantList) {
            obj.obtainMerchantName();
            obj.obtainProductList();
        }
        return merchantList;
    }

    public static Merchant findMerchantDetailByMerchantId(Long merchantId){
        Merchant merchant = find.byId(merchantId);
        merchant.obtainProductList();
        merchant.obtainMerchantName();
        merchant.obtainMerchantWalletId();


        return merchant;
    }

    public static Merchant findMerchantDetailByUserId(Long userId){
        Merchant merchant = find.query().where().eq("user_id",userId).findOne();
        merchant.obtainProductList();

        return merchant;
    }
    public void obtainMerchantWalletId() {
        Users users = Users.findById(userId);
        walletId = users.getWalletId();
    }

    public void obtainProductList(){
        productList = Product.findByMerchantId(merchantId);
    }

    public void obtainMerchantName(){
        name = Users.findById(userId).getName();
    }

    public boolean isHasSeenByAdmin() {
        return hasSeenByAdmin;
    }

    public void setHasSeenByAdmin(boolean hasSeenByAdmin) {
        this.hasSeenByAdmin = hasSeenByAdmin;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public Long getWalletId() {
        return walletId;
    }
}
