package models.user.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.user.Users;

import java.io.IOException;

public class UserSerializer extends StdSerializer<Users> {

    public UserSerializer(){
        this(null);
    }

    protected UserSerializer(Class<Users> t) {
        super(t);
    }

    @Override
    public void serialize(Users users, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("user_id",users.getId());
        jsonGenerator.writeStringField("email",users.getEmail());
        if(users.getWalletId() != null){
            jsonGenerator.writeNumberField("wallet_id",users.getWalletId());
            jsonGenerator.writeNumberField("balance",users.getBalance().longValue());
        }
        jsonGenerator.writeStringField("name",users.getName());
        jsonGenerator.writeStringField("type", users.getType());
        jsonGenerator.writeObjectField("details",users.getUserDetail());
        jsonGenerator.writeEndObject();
    }
}
