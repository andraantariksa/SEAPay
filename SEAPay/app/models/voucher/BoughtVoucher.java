package models.voucher;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Table(name = "boughtVoucher")
public class BoughtVoucher extends Model {
    @Id
    private Long boughtVoucherId;

    private Long customerId;
    private Long voucherId;

    @Transient
    //Doesn't make a collumn
    private transient Voucher details;

    private boolean hasBeenUsed = false;
    private static final Finder<Long, BoughtVoucher> find = new Finder<>(BoughtVoucher.class);

    public BoughtVoucher(Long customerId, Long voucherId) {
        this.customerId = customerId;
        this.voucherId = voucherId;
    }

    public static List<BoughtVoucher> findVoucherByCustomerId(Long customerId){
        List<BoughtVoucher> boughtVoucherList = find.query().where().eq("customer_id",customerId).findList();
        for (BoughtVoucher obj :
                boughtVoucherList) {
            obj.obbtainVoucherDetail();
        }

        return boughtVoucherList;
    }

    public void obbtainVoucherDetail(){
        details = Voucher.findByVoucherId(voucherId);
    }

    public Long getBoughtVoucherId() {
        return boughtVoucherId;
    }

    public void setBoughtVoucherId(Long boughtVoucherId) {
        this.boughtVoucherId = boughtVoucherId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public boolean isHasBeenUsed() {
        return hasBeenUsed;
    }

    public void setHasBeenUsed(boolean hasBeenUsed) {
        this.hasBeenUsed = hasBeenUsed;
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }
}
