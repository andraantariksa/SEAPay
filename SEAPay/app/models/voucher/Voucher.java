package models.voucher;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.ebean.Finder;
import io.ebean.Model;
import models.voucher.serializer.VoucherSerializer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "voucher")
@JsonSerialize(using = VoucherSerializer.class) //For customize the JSON key
public class Voucher extends Model {

    @Id
    private Long voucherId;

    private String name;
    private int value;
    private String description;
    private String type;
    private Long price;

    public Voucher(String name, int value, String description, String type, Long price) {
        this.name = name;
        this.value = value;
        this.description = description;
        this.type = type;
        this.price = price;
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    private static final Finder<Long, Voucher> find = new Finder<>(Voucher.class);

    public static Voucher findByVoucherId(Long voucherId){
        return find.query().where().eq("voucher_id",voucherId).findOne();
    }

    public static Voucher findByName(Long voucherName){
        return find.query().where().eq("name", voucherName).findOne();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
