package models.transaction.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.transaction.Transaction;
import util.Singleton;

import java.io.IOException;

public class TransactionSerializer extends StdSerializer<Transaction> {

    public TransactionSerializer(){
        this(null);
    }

    protected TransactionSerializer(Class<Transaction> t) {
        super(t);
    }

    @Override
    public void serialize(Transaction transaction, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeNumberField("transactionId",transaction.getTransactionId());
                if(transaction.getCreditedWalletId() != null) {
                    jsonGenerator.writeNumberField("creditedWalletId",transaction.getCreditedWalletId());
                }
                if(transaction.getDebitedWalletId() != null) {
                    jsonGenerator.writeNumberField("debitedWalletId",transaction.getDebitedWalletId());
                }
                jsonGenerator.writeStringField("type",transaction.getType());
                jsonGenerator.writeObjectField("details",transaction.getTransactionDetail());
                jsonGenerator.writeStringField("date",transaction.getCreatedDateTime().format(Singleton.getDateTimeFormatter()));
                jsonGenerator.writeEndObject();
    }
}
