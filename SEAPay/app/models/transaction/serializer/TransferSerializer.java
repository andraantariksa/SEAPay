package models.transaction.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.transaction.Transfer;

import java.io.IOException;

public class TransferSerializer extends StdSerializer<Transfer> {
    public TransferSerializer(){
        this(null);
    }

    protected TransferSerializer(Class<Transfer> t) {
        super(t);
    }

    @Override
    public void serialize(Transfer transfer, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("ammount",transfer.getAmmount().longValue());
        jsonGenerator.writeStringField("toAccount",transfer.getToEmail());
        jsonGenerator.writeStringField("fromAccount",transfer.getFromEmail());
        jsonGenerator.writeEndObject();
    }
}
