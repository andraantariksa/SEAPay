package models.transaction.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.transaction.Payment;

import java.io.IOException;

public class PaymentSerializer extends StdSerializer<Payment> {
    public PaymentSerializer(){
        this(null);
    }

    protected PaymentSerializer(Class<Payment> t) {
        super(t);
    }

    @Override
    public void serialize(Payment payment, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("customerPrice",payment.getCustomerPrice().longValue());
        jsonGenerator.writeNumberField("realPrice",payment.getRealPrice().longValue());
        if(payment.getVoucherId() != null){
            jsonGenerator.writeNumberField("voucherId",payment.getVoucherId());
            jsonGenerator.writeStringField("voucherType",payment.getVoucherType());
        }
        jsonGenerator.writeStringField("merchantName",payment.getMerchantName());
        jsonGenerator.writeEndObject();
    }
}
