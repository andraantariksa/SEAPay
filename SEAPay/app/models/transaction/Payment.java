package models.transaction;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.ebean.Finder;
import io.ebean.Model;
import models.transaction.serializer.PaymentSerializer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "payment")
@JsonSerialize(using = PaymentSerializer.class)
public class Payment extends Model implements TransactionDetail {

    @Id
    private Long paymentId;

    private Long transactionId;
    private Long voucherId;
    private String voucherType;
    private String merchantName;
    private Long merchantId;

    private BigDecimal realPrice; //This will be showed in merchant
    private BigDecimal customerPrice; //This will be showed in customer

    private static final Finder<Long,Payment> find = new Finder<>(Payment.class);

    public Payment(Long transactionId, Long voucherId, String voucherType, BigDecimal realPrice, BigDecimal customerPrice,Long merchantId, String merchantName) {
        this.transactionId = transactionId;
        this.voucherId = voucherId;
        this.voucherType = voucherType;
        this.realPrice = realPrice;
        this.customerPrice = customerPrice;
        this.merchantName = merchantName;
        this.merchantId = merchantId;
    }

    public static Payment findByTransactionId(Long transactionId){
        return find.query().where().eq("transaction_id",transactionId).findOne();
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public BigDecimal getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }

    public BigDecimal getCustomerPrice() {
        return customerPrice;
    }

    public void setCustomerPrice(BigDecimal customerPrice) {
        this.customerPrice = customerPrice;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public String getMerchantName() {
        return merchantName;
    }
}
