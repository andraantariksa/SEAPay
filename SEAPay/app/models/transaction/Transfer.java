package models.transaction;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.ebean.Finder;
import io.ebean.Model;
import models.transaction.serializer.TransferSerializer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "transfer")
@JsonSerialize(using = TransferSerializer.class)
public class Transfer extends Model implements TransactionDetail {
    @Id
    private Long transferId;

    private Long transactionId;
    private BigDecimal ammount;
    private String toEmail;
    private String fromEmail;
    private static final Finder<Long,Transfer> find = new Finder<>(Transfer.class);

    public Transfer(Long transactionId, BigDecimal ammount, String toEmail, String fromEmail) {
        this.transactionId = transactionId;
        this.ammount = ammount;
        this.toEmail = toEmail;
        this.fromEmail = fromEmail;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public void setAmmount(BigDecimal ammount) {
        this.ammount = ammount;
    }

    public static Transfer findByTransactionId(Long transactionId){
        return find.query().where().eq("transaction_id",transactionId).findOne();
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }
}
