package models.transaction;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import models.transaction.serializer.TransactionSerializer;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Entity
@Table(name = "transaction")
@JsonSerialize(using = TransactionSerializer.class)
public class Transaction extends Model{

    @Id
    private Long transactionId;

    private Long creditedWalletId; //From User
    private Long debitedWalletId; //To User

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false,updatable = false)
    private LocalDateTime createdAt = LocalDateTime.now();

    private String type;

    @Transient
    private transient TransactionDetail transactionDetail;

    private static final Finder<Long,Transaction> find = new Finder<>(Transaction.class);

    public Transaction(Long creditedWalletId,Long debitedWalletId, String type) {
        this.creditedWalletId = creditedWalletId;
        this.debitedWalletId = debitedWalletId;
        this.type = type;
    }

    public static Transaction findById(Long transactionId){
        return find.query().where().eq("transaction_id",transactionId).findOne();
    }

    public static List<Transaction> findByCreditedWalletId(Long creditedWalletId){
        return find.query().where().eq("credited_wallet_id",creditedWalletId).findList();
    }

    public static List<Transaction> findByDebitedWalletId(Long debitedWalletId){
        return find.query().where().eq("debited_wallet_id",debitedWalletId).findList();
    }

    public static ArrayList<Transaction> findHistoryTransactionByWalletId(Long walletId){
        List<Transaction> creditedList = Transaction.findByCreditedWalletId(walletId);
        List<Transaction> debitedList = Transaction.findByDebitedWalletId(walletId);
        List<Transaction> historyTransaction = new ArrayList<>(creditedList);
        historyTransaction.addAll(debitedList);

        //add transactionDetails
        for (Transaction obj:historyTransaction){
            obj.obtainTransactionDetail();
        }

        //Sorting by date
        historyTransaction = historyTransaction.stream().sorted(Comparator.comparing(Transaction::getCreatedDateTime).reversed()).collect(Collectors.toList());

        return (ArrayList<Transaction>) historyTransaction;
    }

    public void obtainTransactionDetail(){
        switch (type.toLowerCase()){
            case "payment":
                this.transactionDetail = Payment.findByTransactionId(transactionId);
                break;
            case "topup":
                this.transactionDetail = TopUp.findByTransactionId(transactionId);
                break;
            case "transfer":
                transactionDetail = Transfer.findByTransactionId(transactionId);
                break;
            case  "cashback":
                transactionDetail = Cashback.findByTransactionId(transactionId);
                break;
            default:
                this.transactionDetail = null;
                break;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public LocalDateTime getCreatedDateTime() {
        return createdAt;
    }

    public Long getCreditedWalletId() {
        return creditedWalletId;
    }

    public Long getDebitedWalletId() {
        return debitedWalletId;
    }

    public TransactionDetail getTransactionDetail() {
        return transactionDetail;
    }
}
