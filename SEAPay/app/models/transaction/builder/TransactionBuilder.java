package models.transaction.builder;

import models.transaction.Transaction;

public class TransactionBuilder {
    private Long creditedWalletId;
    private Long debitedWalletId;
    private String type;

    public TransactionBuilder setCreditedWalletId(Long creditedWalletId) {
        this.creditedWalletId = creditedWalletId;
        return this;
    }

    public TransactionBuilder setDebitedWalletId(Long debitedWalletId) {
        this.debitedWalletId = debitedWalletId;
        return this;
    }

    public TransactionBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public Transaction createTransaction() {
        return new Transaction(creditedWalletId, debitedWalletId, type);
    }
}