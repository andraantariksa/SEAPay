package forms;

import play.data.validation.Constraints;

public class CartProductForm {
    @Constraints.Required
    private Long productId;

    @Constraints.Required
    private int ammount;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public int getAmmount() {
        return ammount;
    }

    public void setAmmount(int ammount) {
        this.ammount = ammount;
    }
}
