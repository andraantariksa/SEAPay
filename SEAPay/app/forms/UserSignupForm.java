package forms;

import play.data.validation.Constraints;


public class UserSignupForm {
    @Constraints.Required()
    @Constraints.Email()
    private String email;

    @Constraints.Required()
    @Constraints.MinLength(6)
    //@Constraints.Pattern("")
    private String password;

    @Constraints.Required()
    private String type;

   @Constraints.Required()
   private  String name;




    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() { return type; }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}