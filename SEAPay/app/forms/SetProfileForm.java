package forms;

import play.data.validation.Constraints;


public class SetProfileForm {
    // @Constraints.Required()
    // @Constraints.Email()
    // private String email;

    @Constraints.Required()
    @Constraints.MinLength(6)
    //@Constraints.Pattern("")
    private String oldPassword;

    @Constraints.Required()
    @Constraints.MinLength(6)
    //@Constraints.Pattern("")
    private String newPassword;

    // Only for merchant typed user
    private String description;

    @Constraints.Required()
    private  String name;

    public String getNewPassword() {
        return this.newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return this.oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}