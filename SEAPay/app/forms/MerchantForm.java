package forms;

import play.data.validation.Constraints;

public class MerchantForm extends UserSignupForm {
    @Constraints.Required()
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
