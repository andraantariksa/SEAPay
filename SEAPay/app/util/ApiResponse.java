package util;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.Gson;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.IOException;
import java.time.LocalDateTime;

@JsonSerialize(using = ApiResponseSerializer.class)
public class ApiResponse {
    public boolean success;
    public Object data = null;
    public String message;

    private static ApiResponse SINGLETON = null;

    private ApiResponse(){}

    public boolean isSuccess() {
        return success;
    }

    public Object getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public static ApiResponse getInstance(){
        if (SINGLETON == null){
            SINGLETON = new ApiResponse();
        }
        return SINGLETON;
    }

    public JsonNode toJson() {
        ObjectMapper objectMapper = ObjectMapperSingleton.getInstance();
        JsonNode jsonNode = objectMapper.valueToTree(this);
        this.success=false;
        this.data=null;
        this.message="";
        return jsonNode;
    }
}