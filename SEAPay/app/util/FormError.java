package util;

import play.data.Form;
import play.data.validation.ValidationError;

import java.util.List;

public class FormError {
    public static <T> String getErrorMessage(Form<T> form) {
        List<ValidationError> validationErrorList = form.errors();

        String errorMessage = "";
        for (ValidationError obj : validationErrorList){
            errorMessage += String.format(" %s %s %s,", obj.key(),obj.message(),(obj.arguments().toArray().length == 0 ? "" : obj.arguments() )); //Formatting error message
        }

        return errorMessage;
    }
}