package util;

import java.time.format.DateTimeFormatter;

public class Singleton {
    private static String dateTimePattern = "dd-MM-yyyy HH:mm:ss";
    private static DateTimeFormatter dateTimeFormatter = null;

    public static DateTimeFormatter getDateTimeFormatter(){
        if(dateTimeFormatter == null){
            dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimePattern);
        }
        return dateTimeFormatter;
    }
}
