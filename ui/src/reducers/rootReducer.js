const initState = {
    isLoggedIn: false,
    type:"",
    cart:[],
    user_detail:{
        name : "",
        user_id:0,
        sea_point:0,
        loyality_point:0,
        email:"",
        description:"",
        balance:0,
        level:"",
        voucher_list:[],
        product_list:[],
        merchant_proposal_list:[],
        history_transaction:[]
    },
    voucher_shop_list:[],
    merchant_detail:{
    },
    merchant_list : []
  }

const rootReducer = (state = initState ,action) => {
    switch (action.type) {
        case "ADD_TO_CART":
            if (action.detail.ammount !== 0){
                return {
                    ...state,
                    cart:[...state.cart,action.detail]
                }
            }
            return state;
        case "CLEAR_CART":
            return {
                ...state,
                cart:[]
            }
        case "UPDATE_TO_CART":
            let tempCart = state.cart.map(obj => {
                if(action.detail.product_id === obj.product_id){
                    return action.detail
                }else{
                    return obj
                }
            });
            tempCart = tempCart.filter(obj => {
                return obj.ammount !== 0;
            })
            return {
                ...state,
                cart:tempCart
            }
        case "UPDATE_PRODUCT_STOCK":
            let tempProduct = state.merchant_detail.product_list.map(obj => {
                if(action.detail.product_id === obj.product_id){
                    return action.detail
                }else{
                    return obj
                }
            })
            return {
                ...state,
                merchant_detail:{
                    ...state.merchant_detail,
                    product_list:tempProduct
                }
            }
        case "UPDATE_PRODUCT_LIST":
            return {
                ...state,
                user_detail:{
                    ...state.user_detail,
                    product_list:action.detail
                }
            }
        case "UPDATE_PROFILE":
            if(action.detail.data.description === undefined){
                return {
                    ...state,
                    user_detail:{
                        ...state.user_detail,
                        name:action.detail.data.name,
                        description:action.detail.data.details.description
                    }
                }
            }else{
                return {
                    ...state,
                    user_detail:{
                        ...state.user_detail,
                        name:action.detail.data.name,
                    }
                }
            }
        case "UPDATE_VOUCHER_LIST":
            return {
                ...state,
                user_detail:{
                    ...state.user_detail,
                    voucher_list:action.detail
                }
            }
        case "UPDATE_MERCHANT_PROPOSAL_LIST":
            return {
                ...state,
                user_detail:{
                    ...state.user_detail,
                    merchant_proposal_list:action.detail
                }
            }
        case "UPDATE_MERCHANT_DETAIL":
            return {
                ...state,
                merchant_detail:action.detail
            }
        case "LOAD_USER_DETAIL_IN":
            return {
                ...state,
                user_detail:{
                    ...state.user_detail,
                    ...action.detail
                }
            }
        case "LOG_USER_IN":
            switch (action.detail.data.type) {
                case "Customer":
                    let level = null;
                    switch (action.detail.data.details.level) {
                        case 0:
                            level = "Silver"
                            break;
                        case 1:
                            level = "Gold"
                            break;
                        case 2:
                            level = "Platinum"
                            break;
                        default:
                            break;
                    }
                    return {
                            ...state,
                            isLoggedIn:true,
                            type:action.detail.data.type,
                            user_detail:{
                                ...state.user_detail,
                                ...action.detail.data,
                                sea_point:action.detail.data.details.seaPoint,
                                loyality_point:action.detail.data.details.loyalityPoint,
                                level:level,
                                voucher_list:action.detail.data.details.voucherList,
                                customer_id:1
                            }
                        }
                case "Merchant":
                    return {
                        ...state,
                        isLoggedIn:true,
                        type:action.detail.data.type,
                        user_detail:{
                            ...state.user_detail,
                            ...action.detail.data,
                            product_list:action.detail.data.details.product_list,
                            description:action.detail.data.details.description,
                            merchant_id:action.detail.data.details.merchant_id
                        } 
                    }
                case "Admin":
                    return{
                        ...state,
                        isLoggedIn:true,
                        type:action.detail.data.type,
                        user_detail:{
                            ...state.user_detail,
                            ...action.detail.data,
                            voucher_list:action.detail.data.details.voucher_list,
                            merchant_proposal_list:action.detail.data.details.merchant_proposal
                        }
                    }
                default:
                    return; 
            }
        case "UPDATE_TRANSACTION_HISTORY":
            return{
                ...state,
                user_detail:{
                    ...state.user_detail,
                    history_transaction:action.data
                }
            }
        case "UPDATE_BALANCE":
            return {
                ...state,
                user_detail:{
                    ...state.user_detail,
                    balance:action.data.balance
                }
            }
        case "UPDATE_MERCHANT_LIST":
            return{
                ...state,
                merchant_list:action.detail
            }
        case "LOG_USER_OUT":
            return {
                ...state,
                isLoggedIn:false,
                user_detail:{
                    name : "",
                    user_id:null,
                    sea_point:0,
                    loyality_point:0,
                    description:"",
                    balance:0,
                    level:"",
                    voucher_list:[],
                    product_list:[],
                    merchant_proposal_list:[],
                    history_transaction:[]
                }
            }
        default:
            return state;
    }
}

/*
Cart Action contract
     {
        type:"",
        detail: {
            name:
            price:
            ammount:
            totalPrice:
            stock:
            product_id:
        }
     }
Product Action contract
    {
        type:
        detail:{
            name:,
            description:,
            price:,
            stock:,
            merchant:,
            product_id:,
        }
    }
*/
/* 
User Login contract 
    {
        type:
        detail:{
            type:
            user_id:
        }
    }
*/

export default rootReducer;