import React,{Component} from 'react';
import { Card } from 'react-bootstrap';
import MerchantCard from './merchantCard';
import {connect} from 'react-redux';

class MerchantListCard extends Component{
    render(){
        const cardStyle = {
            width:"25rem",
            "border" : "grey solid 1.2pt",
            height:"44rem"
        }
        return(
            <Card style={cardStyle} className="mx-auto">
                <Card.Body>
                    <div className="scroll-hover" style={{height:"36rem", display:"flex", flexDirection:"column"}} >
                        {this.props.merchantList.map(obj => {
                            return (
                                <MerchantCard merchantDetail={obj} key={obj.merchant_id} />
                            )
                        })}
                    </div>
                </Card.Body>
            </Card>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        merchantList:state.merchant_list
    }
}

export default connect(mapStateToProps)(MerchantListCard);