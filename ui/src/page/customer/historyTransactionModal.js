import React from 'react';
import {Modal} from 'react-bootstrap';
import {connect} from 'react-redux';

const HistoryTransactionModal = ({type,walletId,history,show,onHide}) => {
    return(
        <Modal scrollable show={show} onHide={onHide}>
            <Modal.Header closeButton>
                <Modal.Title>History Transaction</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {history.length !== 0 ? history.map(obj => {
                    if(obj.type === "Payment"){
                        return (
                            <div key={obj.transactionId} style={{marginBottom:10,borderBottom:"black solid"}}>
                                <h5>{obj.type}</h5>
                                <h5>Transaction Id : {obj.transactionId}</h5>
                                <h5>Ammount : {(walletId === obj.creditedWalletId ? "+" : "-") + 
                                    (type === "Customer" ? obj.details.customerPrice : obj.detail.realPrice)}</h5>
                                <h5>Merchant : {obj.details.merchantName}</h5>
                                <h5>Date : {obj.date}</h5>
                            </div>
                        )
                    }else if(obj.type === "Transfer"){
                        return(
                            <div key={obj.transactionId} style={{marginBottom:10,borderBottom:"black solid"}}>
                                <h5>{obj.type}</h5>
                                <h5>Transaction Id : {obj.transactionId}</h5>
                                <h5>{(walletId === obj.creditedWalletId ? "From : " + obj.details.fromAccount : "To : " + obj.details.toAccount)}</h5>
                                <h5>Ammount : {(walletId === obj.creditedWalletId ? "+" : "-") + obj.details.ammount}</h5>
                                <h5>Date : {obj.date}</h5>
                            </div>
                        )
                    }else{
                        return(
                            <div key={obj.transactionId} style={{marginBottom:10,borderBottom:"black solid"}}>
                                <h5>{obj.type}</h5>
                                <h5>Transaction Id : {obj.transactionId}</h5>
                                <h5>Ammount : {(walletId === obj.creditedWalletId ? "+" : "-") + obj.details.ammount}</h5>
                                <h5>Date : {obj.date}</h5>
                            </div>
                        )
                    }
                }) : "No History Transaction"}
            </Modal.Body>
        </Modal>
    )
}

const mapStateToProps= (state)=>{
    return {
        walletId:state.user_detail.wallet_id,
        history:state.user_detail.history_transaction,
        type:state.type
    }
}

export default connect(mapStateToProps)(HistoryTransactionModal)