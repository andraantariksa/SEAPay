import React from 'react';
import { Modal} from 'react-bootstrap';

const ProductDetailModal = ({productDetail,show,onHide}) => {
    const styleText ={
        fontWeight:"normal",
        overflowWrap:"break-word"
    }
    return(
        <Modal show={show} onHide={onHide} aria-labelledby="contained-modal-title-vcenter"
      centered scrollable>
            <Modal.Header closeButton>
                <Modal.Title>{productDetail.name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                    <h4 style={styleText}>Stock: {productDetail.stock} Units</h4>
                    <h4 style={styleText}>Price: Rp. {productDetail.price}</h4>
                    <h4 style={styleText}>Description: {productDetail.description}</h4>
            </Modal.Body>
        </Modal>
    )
}
export default ProductDetailModal;