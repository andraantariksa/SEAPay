import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const MerchantDetailModal = ({merchantDetail,show,onHide,accept,decline}) => {
    const styleText ={
        fontWeight:"normal",
        overflowWrap:"break-word"}
    return(
        <Modal show={show} onHide={onHide} aria-labelledby="contained-modal-title-vcenter"
      centered>
            <Modal.Header closeButton>
                <Modal.Title>{merchantDetail.name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <h4 style={styleText}>Description: {merchantDetail.description}</h4>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={accept} variant="success">Accept</Button>
                <Button onClick={decline} variant="danger">Reject</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default MerchantDetailModal;