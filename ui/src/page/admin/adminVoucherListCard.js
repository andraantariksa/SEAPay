import React,{Component} from 'react';
import {Card} from 'react-bootstrap';
import {VoucherCard} from './index'


class VoucherListCard extends Component{
    render(){
        const voucherListStyle = {
            "border" : "grey solid 1.2pt",
            "width":"25rem",
            height:"40rem"
        };
        return(
            <Card style={voucherListStyle} className="mx-auto">
                <Card.Body>
                        <div className="scroll-hover" style={{height:"36rem", display:"flex", flexDirection:"column"}}>
                        {this.props.voucherList.map(obj => {
                            return(
                                    <VoucherCard key={obj.voucher_id}  voucherDetail={obj} removeFunc={() => {this.props.removeFunc(obj.voucher_id)}} />                                    
                            );
                        })}
                        </div>
                </Card.Body>
            </Card>
        );
    }
}

export default VoucherListCard;