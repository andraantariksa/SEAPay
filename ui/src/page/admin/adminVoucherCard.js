import React,{useState} from 'react';
import { Card, Row, Col} from "react-bootstrap";
import VoucherDetailModal from './voucherDetailModal';

const VoucherCard = ({voucherDetail,removeFunc}) => {
    const [ShowModal, setShowModal] = useState(false);
    const styleVoucher={
        "border" : "grey solid 1.2pt",
        "width":"21rem",
        height:"auto"};
    return(
        <div>
            <Card style={styleVoucher} className="mt-2 mx-auto">
                <Card.Body  className="p-3">
                    <Row className="mb-2">
                        <Col md={7} className="align-self-center mb-3">
                            <Card.Title className="mb-0"><button onClick={() => {setShowModal(true)}} className="hover-grey no-style-button" style={{fontWeight:500}}>{voucherDetail.title}</button></Card.Title>
                        </Col>
                        <Col md={{span:3,offset:3}} className="pr-1 pl-3 mb-3 ml-5 align-self-center">
                            <button onClick={removeFunc} className="hover-grey p-0 no-style-button">Remove</button>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="pr-0 align-self-center">
                            <Card.Text>{voucherDetail.price} SEA Point</Card.Text>
                        </Col>
                        <Col className="pr-0 pl-5 align-self-center">
                            <Card.Subtitle className="mt-0">{voucherDetail.type+" : "+voucherDetail.ammount+"%"}</Card.Subtitle>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
            <VoucherDetailModal removeFunc={removeFunc} voucherDetail={voucherDetail} show={ShowModal} onHide={() => {setShowModal(false)}} />
        </div>
    );
}

export default VoucherCard;