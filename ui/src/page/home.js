import React from 'react';
import { Row, Container } from 'react-bootstrap';

function Home() {
    return (
        <Container>
            <Row className="justify-content-center">
                <h1>Welcome to SEA Pay!</h1>
            </Row>
        </Container>
    );
}

export default Home;
