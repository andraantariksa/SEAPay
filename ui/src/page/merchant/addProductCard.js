import React,{Component} from 'react';
import { Card, InputGroup, Form,Row,Col,Button } from 'react-bootstrap';
import {connect} from 'react-redux';
import Axios from 'axios';

class AddProductCard extends Component{
    state={
        name:"",
        price:0,
        desc:"",
        stock:0,
        validated: false
    }
    handleInput = (e) => {
        const isNumber = (text) => {return /^\d+$/.test(text);} //check number or not
        this.setState({
            [e.target.id] : (isNumber(e.target.value) ? parseInt(e.target.value) : e.target.value)
        })
    }
    handleSubmit = (e) => {
        const form = e.currentTarget;
        if (form.checkValidity() === false){
          e.preventDefault();
          e.stopPropagation();
          this.setState({
            validated: true
          })
        }else{
          e.preventDefault();
          e.stopPropagation();
          Axios.post("/api/product/create",{
              "merchantId":this.props.merchantId,
              "name":this.state.name,
              "description":this.state.desc,
              "price":this.state.price,
              "stock" : this.state.stock
          })
          .then(res => {
              this.props.updateProductList(res.data.data);
          })
          .catch(error => {
              console.log(error.response)
          })
          this.setState({
            name:"",
            price:0,
            desc:"",
            stock:0,
            validated: false
          })
        }
      }

    render() {
        const cardAddVoucherStyle = {
            "border" : "grey solid 1.2pt",
            "width":"24rem"};
        return(
            <Card className = "mt-3 mx-auto" style={cardAddVoucherStyle}>
                <Card.Body className="pt-3">
                        <Card.Title className="text-center">Add Product</Card.Title>
                        <Form noValidate validated={this.state.validated} onSubmit={this.handleSubmit} className="mt-2">
                            <Form.Group controlId="name">
                                <Form.Label>Name :</Form.Label>
                                <Form.Control required onChange={this.handleInput} type="text" placeholder="Product Name" value={this.state.name} />
                                <Form.Control.Feedback type="invalid">Product name required!</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group controlId="desc">
                                <Form.Label>Description :</Form.Label>
                                <Form.Control required style={{height:"6rem"}} onChange={this.handleInput} as="textarea" type="text" placeholder="Description" value={this.state.desc}/>
                                <Form.Control.Feedback type="invalid">Must have description!</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group as={Row} controlId="price">
                                <Form.Label column sm="3" className="pr-0">Price :</Form.Label>
                                <InputGroup as={Col} sm="9" className="pl-0">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text id="basic-addon2">Rp.</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <Form.Control style={{borderTopRightRadius:"5px",borderBottomRightRadius:"5px"}} required  type="number" className="no-spin-button" onChange={this.handleInput} min={1} value={this.state.price}/>
                                        <Form.Control.Feedback type="invalid">Minimal price is 1!</Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group as={Row} controlId="stock">
                                <Form.Label column sm="3" className="pr-0">Stock :</Form.Label>
                                <InputGroup as={Col} sm="6" className="pl-0">
                                    <Form.Control required type="number" min="1" onChange={this.handleInput} value={this.state.stock} />
                                    <InputGroup.Append>
                                        <InputGroup.Text style={{borderTopRightRadius:"5px",borderBottomRightRadius:"5px"}} id="basic-addon2">Units</InputGroup.Text>
                                    </InputGroup.Append>
                                    <Form.Control.Feedback type="invalid">Minimal stock is 1!</Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Button className="float-right" variant="primary" type="submit">
                                Add
                            </Button>
                        </Form>
                </Card.Body>
            </Card>
        )
    }
}

const mapStateToProps=(state) => {
    return {
        merchantId:state.user_detail.merchant_id,
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        updateProductList : (obj) => {dispatch({type:"UPDATE_PRODUCT_LIST",detail:obj})}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AddProductCard);