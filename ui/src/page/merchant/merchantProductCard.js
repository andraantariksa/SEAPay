import React,{useState} from 'react';
import { Card, Row,Col } from "react-bootstrap";
import ProductDetailModal from './productDetailModal';

const ProductCard = ({productDetail,removeFunc}) => {
    const [ShowModal, setShowModal] = useState(false);
    const wrappedRemoveFunc = () => {
        removeFunc();
        setShowModal(false);
    }
    const productDescription = (productDetail.description.length > 80 ? productDetail.description.slice(0,80) + "...." : productDetail.description );
    const styleProduct={
        "border" : "grey solid 1.2pt",
        "width":"20rem",
        maxHeight:"14rem",
    display:"inline-block",
        margin:"8px"};
    return(
        <Card style={styleProduct}>
            <Card.Body>
                <Row className="mb-2">
                    <Col md={7} className="align-self-center">
                        <Card.Title className="mb-0"><button onClick={() => {setShowModal(true)}} className="p-0 hover-grey no-style-button" style={{fontWeight:"bold"}}>{productDetail.name}</button></Card.Title>
                    </Col>
                    <Col md={{span:2,offset:1}} className="align-self-center">
                        <button onClick={removeFunc} className="hover-grey p-0 no-style-button">Remove</button>
                    </Col>
                </Row>
                <Card.Text style={{height:"4.5rem"}}>{productDescription}</Card.Text>
            </Card.Body>
            <Card.Body>
                <Row className="justify-content-center">
                    <Col xs={5} className="align-self-center p-0 text-center">
                        <Card.Subtitle style={{fontSize:"14pt"}}>{"Rp. "+productDetail.price}</Card.Subtitle>
                    </Col>
                    <Col xs={{offset:1,span:6}} className="align-self-center p-0 text-center">
                        <h6 className="mb-0">Stock : {productDetail.stock} Units</h6>
                    </Col>
                </Row>
            </Card.Body>
            <ProductDetailModal productDetail={productDetail} removeFunc={wrappedRemoveFunc} show={ShowModal} onHide={() => {setShowModal(false)}} />
        </Card>
    )
}

export default ProductCard;