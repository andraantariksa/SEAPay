import React, { Component } from 'react';
import { Row, Container } from 'react-bootstrap';
import { CustomerApp } from './customer/index';
import { AdminApp } from './admin/index';
import { MerchantApp } from './merchant/index';
import {connect} from 'react-redux';

class HomeLoggedIn extends Component{
  render(){
    let appType = null;
    switch (this.props.type) {
      case "Customer":
        appType = (
          <CustomerApp user = {this.props.user} />
        );
        break;
      case "Admin":
        appType = (
          <AdminApp user={this.props.user} />
        );
        break;
      case "Merchant":
        appType = (
          <MerchantApp user={this.props.user}/>
        );
        break;
      default:
        break;
    }
    return (
      <Container fluid className="justify-content-center">
        <Row className="justify-content-center">
            <h2>Welcome to SEA-Pay</h2>
        </Row>
        {appType}
      </Container>  
      )
  }
}

const mapStateToProps = (state) => {
  return {
      name:state.name,
      type:state.type,
  }
}

export default connect(mapStateToProps)(HomeLoggedIn) ;