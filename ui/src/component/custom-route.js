import React from 'react';
import { Route, Redirect} from "react-router-dom";

function PrivateRoute({ render: Render, isAuthenticated, pathFail, ...rest }) {
    return (
        <Route {...rest} render={
            (props) =>
                isAuthenticated ? (
                    <Render {...props} {...rest} />
                ) : (
                    <Redirect to={{
                        pathname: (pathFail === undefined) ? '/signin' : pathFail,
                        state: { from: props.location }
                    }}/>
                )
        } />
    );
}

function PublicPrivateRoute({ publicRender: PublicRender, privateRender: PrivateRender, isAuthenticated, ...rest }) {
    return (
        <Route {...rest} render={
            (props) =>
                isAuthenticated ? (
                    <PrivateRender {...props} {...rest} />
                ) : (
                    <PublicRender {...props} {...rest}/>
                )
        } />
    );
}

export {
    PrivateRoute,
    PublicPrivateRoute,
};