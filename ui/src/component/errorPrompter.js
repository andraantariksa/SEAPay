import React from 'react';
import {Modal} from 'react-bootstrap';

const ErrorModal = ({text,show,onHide}) => {
    return (
        <Modal show={show} onHide={onHide}>
            <Modal.Header closeButton>
                <Modal.Title>Error</Modal.Title> 
            </Modal.Header>
            <Modal.Body>
                {text}
            </Modal.Body>
        </Modal>
    );
}

export default ErrorModal;